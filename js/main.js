
/* ============= ВИДЕО ==============*/

$(document).ready(function () {
		var videobackground = new $.backgroundVideo($('.cont-header'), {
        "align": "centerXY",
        
        "path": "video/",
        "filename": "modelist.fon",
        "types": ["mp4"],
        "preload": true,
        "autoplay": true,
        "loop": true,
		"muted":true
      });
    });

/* ============= КОМАНДА ==============*/
$(document).ready(function(){
    $(".team-descr1").mouseenter(function(){
        $(".team-item1").animate({left: '50%'});
    });
     $(".team-descr1").mouseleave(function(){
        $(".team-item1").animate({left: '0%'});
    });
});
$(document).ready(function(){
    $(".team-descr2").mouseenter(function() {
        $(".team-item2").animate({right: '50%'});
    });
     $(".team-descr2").mouseleave(function(){
        $(".team-item2").animate({right: '0%'});
    });
});
$(document).ready(function(){
    $(".team-descr3").mouseenter(function(){
        $(".team-item3").animate({left: '50%'});
    });
     $(".team-descr3").mouseleave(function(){
        $(".team-item3").animate({left: '0%'});
    });
});
$(document).ready(function(){
    $(".team-descr4").mouseenter(function(){
        $(".team-item4").animate({right: '50%'});
    });
     $(".team-descr4").mouseleave(function(){
        $(".team-item4").animate({right: '0%'});
    });
});	


/* ============= НАДПИСЬ ==============*/
$(function(){
        $("h1").typed({
            strings: ["modelist.web", "Your guide to the it world"],
            typeSpeed: 150,
			backDelay: 2000,
			loop: true,
			loopCount: false,
			 showCursor: false,
			 cursorChar: "|",
        });
    });
/* ============= СЛАЙДЕРЫ ==============*/

$(document).ready(function(){
  $('.sl').slick({
	  autoplay:true,
	  autoplaySpeed:1000,
	  arrows:false,
  });
});





$(document).ready(function(){
  $('.sl1').slick({
	slidesToShow: 3,
  	slidesToScroll: 1,
  	autoplay: true,
  	autoplaySpeed: 5000,
  });
});





$(document).ready(function(){
  $('.sl2').slick({
	  autoplay:true,
	  autoplaySpeed:4000,
	  arrows:true,
  });
});



/* ============= МОДАЛЬНЫЕ ОКНА ==============*/




$(document).ready(function(){
	$('.js-modal').on("click",function(e){
	  	e.preventDefault();
	  	$(".modal,modal-content").fadeIn();
  	});
	
	$('.close').on("click",function(e){
	  	e.preventDefault();
	  	$(".modal,modal-content").fadeOut();
  	});
});





$(document).ready(function(){
	$('.js-modal2').on("click",function(e){
	  	e.preventDefault();
	  	$(".modal2,modal-content2").fadeIn();
  	});
	
	$('.close').on("click",function(e){
	  	e.preventDefault();
	  	$(".modal2,modal-content2").fadeOut();
  	});
});




/* ============= АКАРДИОН ==============*/




$(".js-acr-title").on("click", function(e) {

        e.preventDefault();
        var $this = $(this);

        if( !$this.hasClass("open") ) {
            $(".js-acr-content").slideUp();
            $(".js-acr-title").removeClass("open");
        }

        $this.toggleClass("open");
        $this.next().slideToggle();

    });




/* ============= КАРТА ==============*/





    ymaps.ready(init);
    var myMap,
		myPlacemark;
	
    function init(){     
        myMap = new ymaps.Map("map", {
            center: [46.49142108, 30.73676527],
            zoom: 17,
			controls: ["zoomControl"]
        });
		
		myMap.behaviors.disable(['scrollZoom'
		
		]);
		
		myPlacemark = new ymaps.Placemark([46.49158434, 30.73649949], {
			hintContent: 'Modelist.Web!', 
			balloonContent: 'Hello :)'},{ 
			iconLayout: 'default#image',
        	iconImageHref: 'img/m_logo.png',
        	iconImageSize: [25, 25],
        	iconImageOffset: [-15, -25]
		});
		
		myMap.geoObjects.add(myPlacemark);
    }




    
 
/* ============= АНИМАЦИЯ ==============*/





    new WOW().init();
 



/* ============= ТАБЫ ==============*/




$(document).ready(function($) {
	//скрыть весь контент
	$('.tab_content').hide();
	//Показать контент первой вкладки
	$('.tab_content:first').show();
	//Активировать первую вкладку
	$('.tabs li:first').addClass('active');

	//Событие по клику
	$('.tabs li').click(function(event) {
		event.preventDefault();
		//Удалить "active" класс
		$('.tabs li').removeClass('active');
		//Добавить "active" для выбранной вкладки
		$(this).addClass('active');
		//Скрыть контент вкладки
		$('.tab_content').hide();

		//Найти значение атрибута ссылки, чтобы 
		//определить активный таб контент
		var selectTab = $(this).find('a').attr("href");
		//Исчезновение активного контента
		$(selectTab).fadeIn();
	});
});



/* ============= ПРЕЛОАДЕР ==============*/


		var myVar;

		function myFunction() {
    	myVar = setTimeout(showPage, 000);
		}

		function showPage() {
  		document.getElementById("loader").style.display = "none";
  		document.getElementById("wrap").style.display = "block";
		}


/* ============= ГАМБУРГЕР ==============*/




/* ============= ПЛАВНЫЙ СКРОЛЛ ==============*/


(function(){
			$(window).on("load",function(){
				
				/* Page Scroll to id fn call */
				$(".main_menu a").mPageScroll2id({
					offset:200
				});
			});
		})(jQuery);


(function(){
			$(window).on("load",function(){
				
				/* Page Scroll to id fn call */
				$(".header__nav_paginition a").mPageScroll2id({
					offset:110
				});
			});
		})(jQuery);